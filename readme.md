## Synopsis

This project is to show an example of how to use lkqd ad services inside your android application. 
The demo application is a simple app that will show an ad after a series of button presses.
It also demonstrates loading 2 ads back to back with different methods of starting them.
The demo app should have plenty of comments to follow along and understand what is being called and why. 
All end user functionality is demonstrated in MainActivity.java and any LKQD specific things should be marked as such.
Anything specifically for the demo that isn't needed should have this comment above it: //example app specific code

## Installation

LKQD Ads are built using android studio for Android API 16.

An example of importing is shown here:
![alt tag](http://devflash.lkqd.com/img/lkqd_android_install.gif)

To begin using lkqd ads in your app simply add the lkqdad-release.aar file to your project. 
Follow the instructions here under "Add your library as a dependency":
https://developer.android.com/studio/projects/android-library.html
The first method in step 1 is used. Make sure the gradle files have the correct settings (steps 2,3) and then sync (step 4). 
Do the same for the library LQD-moat-mobile-app-kit-1.7.6-2.aar.
Look at build.gradle (Module: app) and settings.gradle to see what the gradle settings should include. 

## Example Usage

There are 3 ways to instantiate and run a lkqd ad. This is through automation levels. 

Example initialization:
_lkqdad = new LKQDAD(this,this,264,58933,0,600);
//LKQDAD(Activity activity, LKQDDelegate lkqdDelegate, int pid, int sid, int automationLevel, double desiredBitrate)

PID and SID values are unique to each lkqd account and not optional. 
More info here: https://wiki.lkqd.com/display/PUBINT/iOS+SDK 

The default automation level is 1 where the user can initialize an ad and start it once they receive an AdLoaded event. 
Level 0 requires the user to listen to all events and respond to continue to each stage. 
Level 2 is fully automated and the ad will play as soon as possible if nothing fails. 
The desired bitrate (default is 600 kbps) is used only when automation levels 1 and 2 are used.
If you are using level 0 you will still have to pass in the bitrate again on initAd().

The events to listen to are:
webViewLoaded (required for level 0)
AdLoaded (required for level 0 and level 1)

These are received in the lkqdEventReceived function which must be declared on the lkqdAd delegate. 
Delegate functions are required to know what is happening on lkqd ad. 
Most events are funneled through lkqdEventReceived but there are special cases for timeouts and ad ending.

Once the ad moves to a new state the lkqdEventReceived function will relay that event and the user can respond. 

The example app shows level 0 and level 2 being set. The default level 1 is the most common use case. 
Simply instantiate the LKQDAD with automation level 1. 
Listen for the "AdLoaded" event and call startAd() on that instance whenever you wish to show an ad to the user.

For level 0, the app shows how to manually navigate through each event. 
First lkqdAd is created with the first button press and the delegate is assigned.
The 2nd button will appear when it receives the webViewLoaded event in the delegate function lkqdEventReceived. 
When that 2nd button is pressed, the function calls lkqdAd.initAd() with the desired width/height of the ad.
Once initAd() has completed the lkqdEventReceived function will receive AdLoaded and you can press the 3rd button to call startAd()
Automation level 0 is best used where you will have time to load an ad and want a little more control over when loading and requests occur. 
You also have more control over values passed into initAd() like viewMode, creativeData, and environmentVars. 
The example app passes in defaults for these values and unless you have very custom ads you should not need to change this. 

Level 2 is much simpler and demonstrated with lkqdAd2 in the example app. Simply instantiate lkqdAd with automation level 2 and the ad will start as soon as it receives the events. 
It is important to note that you should not handle events that are automated for you since you will be simply calling functions that are already called. 
Some logic is in place to prevent this on LKQDAD classes but it should still be avoided. 
In the example app if you turn on both ads then you will notice there is a time where the first ad disappears and the 2nd ad takes a small amount of time to reappear. 
This is the drawback of automationLevel 2 since there is no way to preload the ad. 
It prepares itself and plays ASAP but it still takes time to load.
This could be avoided by using automation level 1 on the 2nd ad and preparing them both at the same time. 
Then, if you get the 2nd ad's "AdLoaded" event you could wait until the first one ends to call startAd() on the 2nd.

Another important delegate function is lkqdAdTimeOut.
If this is called then the ad has failed to load which could be for a number of reasons like an ad wasn't available or a timeout on the request.
If you receive this event you should null out your ad instance and try again. 
Automation level 1 is good for this because you either receive AdLoaded or lkqdAdTimeOut is called.
If lkqdAdTimeOut is called on an instance with automation level 2 due to it failing then you might miss an opportunity to show an ad.

Finally when an ad ends either through an error or being stopped then there will be a call to the delegate function lkqdAdEnded.
This is where you might want to transition to a new scene and null out your lkqd ad instance. 
It is strongly recommended to null out the lkqd ad instance to make sure the ad calls are unique and prevent errors.

LKQDAD has the full vpaid 2.0 interface available for accessing ad information but it will probably not be necessary to use most of those functions. 
All functions and their use cases can be found here:
https://www.iab.com/wp-content/uploads/2015/06/VPAID_2_0_Final_04-10-2012.pdf
        
## Important Notes

In the example app there are a few modifications to enable better ad tracking. 
Please refer to the AndroidManifest.xml included in this project to see what is being set and why. 

---Required Settings---

Most of the manifest settings required for the example app have been set in the library's manifest file.
android:hardwareAccelerated="true" is required to allow html5 video which LKQD uses for their ad delivery.

These settings are used for gathering location data if it has been allowed by the user/app and enabled in the library.
uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"
uses-permission android:name="android.permission.INTERNET"
uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" 
uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" 

You should not have to add these settings to your app's manifest xml if no other module needs them. 

---Rotation Handling---

If your app will have multiple orientations due to rotating then you should have the following options set. 
For the sample app we set android:screenOrientation="fullSensor" to be able to handle all orientations. 
You can see all the options here: https://developer.android.com/guide/topics/manifest/activity-element.html

Also android:configChanges="orientation|screenSize" should be set on any activity that will be passed to LKQDAD instances.
Those activities should also override onConfigurationChanged which you can see an example of in MainActivity.java
This is required to stop the activity from resetting while an ad is playing and gets rotated. 

You can see these settings in the manifest.xml for the example project. 

## License

© Likqid Media, Inc. - All Rights Reserved.