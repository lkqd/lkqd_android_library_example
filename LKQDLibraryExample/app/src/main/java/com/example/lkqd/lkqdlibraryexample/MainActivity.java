package com.example.lkqd.lkqdlibraryexample;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import lkqd.lkqdad.LKQDAD;
import lkqd.lkqdad.LKQDDelegate;

public class MainActivity extends AppCompatActivity implements LKQDDelegate
{
    //references to lkqd ad instances
    private LKQDAD _lkqdad;
    private LKQDAD _lkqdad2;

    //storing width/height for video size
    private Number _videoWidth;
    private Number _videoHeight;

    //button references to show/hide buttons based on lkqd ad state
    private Button _prepareButton;
    private Button _initButton;
    private Button _startButton;

    //whether or not to load 2 ads for example of loading 2nd ad after first ends
    private boolean _load2Ads = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //example app specific code
        //standard onCreate stuff
        super.onCreate(savedInstanceState);
        //just hides the app label in the main activity view
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        //set button references to show/hide buttons based on lkqd ad state
        _prepareButton = (Button)this.findViewById(R.id.prepareButton);
        _initButton = (Button)this.findViewById(R.id.initButton);
        _startButton = (Button)this.findViewById(R.id.startButton);
        //resets button states
        resetAppState();
    }

    //this function will just reset the buttons to get the app back to its default state
    private void resetAppState()
    {
        _prepareButton.setVisibility(View.VISIBLE);
        _initButton.setVisibility(View.INVISIBLE);
        _startButton.setVisibility(View.INVISIBLE);
    }

    //called when prepare button is pressed
    public void preparePressed(View view)
    {
        //initializing a new lkqdad instance automatically attempts to load a web view in the background
        //the next step is waiting for the webviewloaded event
        _lkqdad = new LKQDAD(this,this,264,58933,0,600);

        //example app specific code
        //debug output
        System.out.println("PREPARE PRESSED");
        //turns off the button to prevent multiple presses
        _prepareButton.setVisibility(View.INVISIBLE);
        //get full screen size for passing to init ad
        Configuration configuration = getResources().getConfiguration();
        _videoWidth = configuration.screenWidthDp;
        _videoHeight = configuration.screenHeightDp;
    }

    //called when init button is pressed
    public void initPressed(View view)
    {
        //call initad with width/height of video
        _lkqdad.initAd(_videoWidth,_videoHeight,"normal",600,"","");

        //example app specific code
        //debug output
        System.out.println("INIT PRESSED");
        //turns off the button to prevent multiple presses
        _initButton.setVisibility(View.INVISIBLE);
    }

    //called when start button is pressed
    public void startPressed(View view)
    {
        //ad should be loaded here, but this is a good check to have
        //this button shouldnt even be visible in this app unless adloaded was called, anyway
        if(_lkqdad.isAdLoaded())
        {
            //once the ad is loaded you can call startAd at any time
            //this will cause a new webview to appear and play an ad immediately
            _lkqdad.startAd();
        }

        //example app specific code
        //debug output
        System.out.println("START PRESSED");
        //turns off the button to prevent multiple presses
        _startButton.setVisibility(View.INVISIBLE);
    }

    //required for handling orientation change on device to prevent resetting activity
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    //LKQD AD SPECIFIC FUNCTION
    //required delegate function which is called when AdStopped or AdError has fired
    //This should be an entry point to change scenes since the ad is no longer visible
    //Or you could play another ad here if so desired
    public void lkqdAdEnded(LKQDAD lkqdAd)
    {
        //example 2nd ad auto starting based on event
        //since the first ad has fired the lkqdAdEnded event we can move onto the next
        if(_load2Ads)
        {
            //multiple ads so check if the lkqdad passed in is the 1st one or the 2nd one
            if(lkqdAd == _lkqdad)
            {
                //null out the first
                _lkqdad = null;

                //start 2nd ad
                //ad should autostart after small loading delay
                _lkqdad2 = new LKQDAD(this, this, 264, 58933, 2, 600);
            }
            //if 2nd ad has ended we come to this function again and null it out as well
            if(lkqdAd == _lkqdad2)
            {
                _lkqdad2 = null;
            }
        }
        else
        {
            //--LKQD--null out lkqd ad when complete.
            //Should reinstance it each time to make sure web view loads with new ad information
            _lkqdad = null;
        }

        //example app specific code
        //debug output
        System.out.println("lkqdAdEnded");
        //ad ended so reset the buttons
        resetAppState();
    }

    //LKQD AD SPECIFIC FUNCTION
    //required delegate function for handling vpaid events from lkqdad
    public void lkqdEventReceived(LKQDAD lkqdAd, String eventString)
    {
        //--LKQD--event fired when the web view is done loading
        if(eventString.equals("webViewLoaded"))
        {
            //if using automation level 0 then this is the first event you need to watch

            //at this point we know the webview is loaded and ready to receive vpaid commands like initAd()
            //turn on init button since it is now OK to call initad()
            _prepareButton.setVisibility(View.INVISIBLE);
            _initButton.setVisibility(View.VISIBLE);
            _startButton.setVisibility(View.INVISIBLE);
        }
        //--LKQD--event fired when lkqdAd.initAd() is called
        if(eventString.equals("AdLoaded"))
        {
            //if using automation level 1 then this is the only event you need to watch
            //if using automation level 0 then this is the 2nd event you need to watch

            //at this point we know the ad is ready to start and is just waiting for the startAd() command
            //also the ad can now be resized and have other vpaid calls to it

            //turn on start button since it is now OK to call startAd()
            _prepareButton.setVisibility(View.INVISIBLE);
            _initButton.setVisibility(View.INVISIBLE);
            _startButton.setVisibility(View.VISIBLE);
        }

        //--LKQD--example of progress event
        //vpaid events will fire as they are received from the ad
        //maybe you want to start loading something in the background since you have time
        if(eventString.equals("AdVideoFirstQuartile"))
        {
        }

        //--LKQD--ad has been clicked and chrome should now be loading their site in the foreground
        //Might want to call stopAd() and transition to the next scene when "lkqdAdEnded" gets called
        if(eventString.equals("AdClickThru"))
        {
            //calling this will cause the ad to end and "lkqdAdEnded" function will be called
            //this is not necessary to call on AdClickThru and is just an example of stopping an ad prematurely
            _lkqdad.stopAd();

            //reset the buttons because stop ad has been called and we want to reset
            resetAppState();
        }

        //example app specific code
        //debug output
        System.out.println("lkqdEventReceived: " + eventString);
    }

    //LKQD AD SPECIFIC FUNCTION
    //required delegate function called after 10 second timeout on initAd() call
    //handle ad not loading properly
    public void lkqdAdTimeOut(LKQDAD lkqdAd)
    {
        //null out instance of lkqd ad (and try again if you want), switch to next scene, etc

        //check for which ad is being passed in and null out the correct one
        if(_load2Ads)
        {
            if(lkqdAd == _lkqdad2)
            {
                _lkqdad2 = null;
            }
            if(lkqdAd == _lkqdad)
            {
                _lkqdad = null;
            }
        }
        else
        {
            //just null out the ad since there are not 2 of them
            _lkqdad = null;
        }

        //example app specific code
        //debug output
        System.out.println("lkqdAdTimeOut");
        //start example app over
        resetAppState();
    }

    //LKQD AD SPECIFIC FUNCTION
    //required delegate function called whenever a value from the ad has changed
    //this function simply alerts the app that a value has changed
    //you still have to request the value from lkqdad and react to it
    public void lkqdStatusChange(LKQDAD lkqdAd, String value)
    {
        //example listening for adRemainingTime change
        if(value.equals("adRemainingTime"))
        {
            //do something more interesting than this
            Number remainingTime = lkqdAd.getAdRemainingTime();
            System.out.println("adRemainingTime = " + remainingTime);
        }

        //example app specific code
        //debug output
        System.out.println("status change for " + value);
    }

}
